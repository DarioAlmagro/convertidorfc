package convertidor.darioalmagro.facci.convertidor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button convertirF, convertirC;
    EditText txtF, txtC;
    TextView resultadoC, resultadoF;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convertirF = (Button) findViewById(R.id.btnFaC);
        convertirC = (Button) findViewById(R.id.btnCaF);
        txtF = (EditText) findViewById(R.id.txtConvertirFaC);
        resultadoC = (TextView) findViewById(R.id.tvResultadoC);
        txtC = (EditText) findViewById(R.id.txtConvertirCaF);
        resultadoF = (TextView) findViewById(R.id.tvResultadoF);



        convertirF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double f = Double.valueOf(txtF.getText().toString());
                double c = ((f-32)*5)/9;
               resultadoC.setText("" + c + " ºC");
                Log.e("Mi APP","Dario Almagro");
            }
        });

        convertirC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double c = Double.valueOf(txtC.getText().toString());
                double r1 = ((c*9)/5)+32;
                resultadoF.setText("" + r1 + " ºF");
                Log.e("Mi APP","Dario Almagro");
            }
        });
    }
}
